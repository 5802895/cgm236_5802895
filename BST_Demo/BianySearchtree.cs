﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BST_Demo
{
   public class BianySearchtree
    {
        public Node root;

        public BianySearchtree()
        {
            root = null;
        }

        public void insert(int i)
        {
            Node newNode = new Node();
            newNode.Data = i;

            if (root == null)
                root = newNode;
            else
            {
                Node current = root;
                Node parent;

                while(true)
                {
                    parent = current;
                    if(i<current.Data)
                    {
                        current = current.Left;
                        if(current==null)
                        {
                            parent.Left = newNode;
                                
                            break;
                        }
                    }

                    else
                    {
                        current = current.Right;
                        if(current==null)
                        {
                            parent.Right = newNode;
                            break;
                        }
                    }
                }
            }

        }

        public void InOrder(Node theRoot)


        {
            if(!(theRoot==null))
            {
                InOrder(theRoot.Left);
                theRoot.DisplayNode();
                InOrder(theRoot.Right);

                
            }
        }

        public void PreOrder(Node theRoot)
        {
            if(!(theRoot==null))
            {
                theRoot.DisplayNode();
                PreOrder(theRoot.Left);
                PreOrder(theRoot.Right);
            }
        }

        public void PostOrder(Node theRoot)
        {
            if(!(theRoot==null))
            {
                PostOrder(theRoot.Left);
                PostOrder(theRoot.Right);
                theRoot.DisplayNode();
            }
        }

        public int FindeMin()
        {
            Node current = root;
            while (!(current.Left == null))
                {
                current = current.Left;
            }

            return current.Data;
        }

        public int FindMax()
        {
            Node current = root;
            while(!(current.Right==null))
            {
                current = current.Right;

            }
            return current.Data;
        }
    }
}
